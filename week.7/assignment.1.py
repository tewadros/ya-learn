# -*- coding: utf-8 -*-
from __future__ import division
import pandas as pd
import numpy as np
from sklearn.cross_validation import KFold
from sklearn.cross_validation import cross_val_score
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import roc_auc_score
import datetime
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression

"""
    Запуск обучения градиентного бустинга
"""
def gb_train(clf, cv, X, y, n_estimators, subsample, max_depth):
    clf.n_estimators = n_estimators
    clf.subsample = subsample
    clf.max_depth = max_depth
    print 'Запуск обучения стохастического градиентного бустинга'
    print 'Параметры классификатора'
    print 'n_estimators:', clf.n_estimators
    print 'subsample:', clf.subsample
    print 'max_depth:', clf.max_depth

    score = 0
    start_time = datetime.datetime.now()
    for train, test in cv:
        X_train, X_test, y_train, y_test = X.values[train], X.values[test], y.values[train], y.values[test]
        clf.fit(X_train, y_train)
        pred = clf.predict_proba(X_test)[:, 1]
        score += roc_auc_score(y_test, pred)

    print 'Затраченное время:', datetime.datetime.now() - start_time
    print 'roc_auc_score:', score / cv.n_folds
    print
    return clf


"""
    Подготавливает данные, запускает обучение GradientBoostingClassifier
    с различными параметрами и подсчитывает качество классификации на кросс-валидации
"""
def gradient_busting_trials(features, subset_length=None):
    subset = features
    
    if subset_length is None:
        subset_length = features.shape[0]
    else:
        print "Получение подвыборки для ускорения обучения"
        subset = features.sample(n=subset_length, random_state=21)
    
    y = subset.radiant_win

    features_to_drop = [
        'radiant_win',
        'barracks_status_dire',
        'barracks_status_radiant',
        'tower_status_radiant',
        'tower_status_dire',
        'duration',
    ]

    X = subset.drop(labels=features_to_drop, axis=1)

    # заполнение пропусков нулями
    X.fillna(value=0, inplace=True)

    N_folds = 5

    cv = KFold(subset_length, n_folds=N_folds, shuffle=True, random_state=21)
    gb = GradientBoostingClassifier(random_state=21)

    for n in range(10, 31, 10):
        gb_train(gb, cv, X, y, n, 1, 3)
        gb_train(gb, cv, X, y, n, 0.3, 3)
        gb_train(gb, cv, X, y, n, 1, 2)
        gb_train(gb, cv, X, y, n, 0.3, 2)

    return
"""
    Выделяет уникальные id героев
"""
def unique_heroes(features, hero_cols):
    heroes = np.array([])
    for col in hero_cols:
        heroes = np.append(heroes, features[col].unique())
    return np.unique(heroes)

"""
    Кодирует id-героев с помощью мешка слов
"""
def encode_heroes(features, hero_cols):
    X_pick = np.zeros((features.shape[0], unique_heroes(features, hero_cols).max()))

    for i, match_id in enumerate(features.index):
        for p in xrange(5):
            X_pick[i, features.ix[match_id, 'r%d_hero' % (p+1)]-1] = 1
            X_pick[i, features.ix[match_id, 'd%d_hero' % (p+1)]-1] = -1
    return X_pick

"""
    Подготавливает данные для обучения/классификации с помощью логистической регрессии
    - удаляет "данные из будущего"
    - заполняет пропуски
    - разбивает на признаки и ответ
    Опционально:
    - удаляет категориальные признаки
    - кодирует id-героев с помощью мешка слов
"""
def lr_prepare_data(features, drop_categorical=False, use_encoded_heroes=False, scaler=None):
    features_to_drop = [
        'radiant_win',
        'barracks_status_dire',
        'barracks_status_radiant',
        'tower_status_radiant',
        'tower_status_dire',
        'duration',
    ]

    hero_cols = [
        'r1_hero',
        'r2_hero',
        'r3_hero',
        'r4_hero',
        'r5_hero',
        'd1_hero',
        'd2_hero',
        'd3_hero',
        'd4_hero',
        'd5_hero',
    ]

    if drop_categorical:
        features_to_drop += ['lobby_type'] + hero_cols

    dropable_features = []
    for col in features_to_drop:
        if col in features.columns:
            dropable_features.append(col)
    
    X = features.drop(labels=dropable_features, axis=1)
    X.fillna(value=0, inplace=True)
    if use_encoded_heroes:
        heroes = encode_heroes(features, hero_cols)
        X = np.hstack((X, heroes))

    if scaler is None:
        scaler = StandardScaler()
        X_scaled = scaler.fit_transform(X)
    else:
        X_scaled = scaler.transform(X)

    y = None
    if 'radiant_win' in features.columns:
        y = features.radiant_win
    return X_scaled, y, scaler


"""
    Запуск обучения логистической регрессии
"""
def lr_train(clf, cv, X, y, C):
    clf.C = C
    print 'Запуск обучения логистической регрессии'
    print 'Параметры классификатора'
    print 'C:', clf.C
    print
    score = 0
    start_time = datetime.datetime.now()
    for train, test in cv:
        X_train, X_test, y_train, y_test = X[train], X[test], y.values[train], y.values[test]
        clf.fit(X_train, y_train)
        pred = clf.predict_proba(X_test)[:, 1]
        score += roc_auc_score(y_test, pred)

    print 'Затраченное время:', datetime.datetime.now() - start_time
    print 'roc_auc_score:', score / cv.n_folds
    print
    return score / cv.n_folds


"""
    Запускает обучение логистической регрессии с кросс-валидацией,
    вычислением ROC AUC и подсчетом времени в трех режимах:
     - без предобработки категориальных признаков
     - без категориальных признаков вообще
     - с кодированием id-героев с помощью мешка слов
"""
def logistic_regression_trials(features):
    cv = KFold(features.shape[0], n_folds=5, shuffle=True, random_state=21)
    
    lr = LogisticRegression(penalty='l2', random_state=21)

    regularization_param_range = np.power(10.0, np.arange(-4, 2))

    
    X, y, _ = lr_prepare_data(features)
    for C in regularization_param_range:
        lr_train(lr, cv, X, y, C)

    print 
    print "Удалим из выборки категориальные признаки"
    print 
    X, y, _ = lr_prepare_data(features, drop_categorical=True)
    for C in regularization_param_range:
        lr_train(lr, cv, X, y, C)

    print
    print "Используем мешок слов для кодирования идентификаторов героев"
    print
    X, y, _ = lr_prepare_data(features, drop_categorical=True, use_encoded_heroes=True)
    for C in regularization_param_range:
        lr_train(lr, cv, X, y, C)

    return

def print_missing_features(features):
    print "Признаки, которые содержат пропуски"
    trainSetLenght = features.shape[0]
    missingValuesCounts = trainSetLenght - features.count()
    for featureName in features.columns[missingValuesCounts != 0]:
        print featureName, missingValuesCounts[featureName]

    print """
    Все отсуствующие признаки описывают время возникновения
    некоторых игровых событий, следовательно могут остуствовать,
    если данное событие не поизошло в первые 5 минут матча
    """
    print
    print


#
#
#
#
#
#    Финальное задание
#
#
#
#
#
#


features = pd.read_csv('features.csv', index_col='match_id')

#print_missing_features(features)

"""
    Подход 1 - градиентный бустинг
"""
#gradient_busting_trials(features)

"""
    Подход 2 - логистическая регрессия
"""
#logistic_regression_trials(features)

features_test = pd.read_csv('features_test.csv', index_col='match_id')

print """по результатам кросс-валидации лучшей моделью оказалась
логистическая регрессия с параметром регуляризации 0.01
удалением категориальных признаков и кодированием id-героев с помощью мешка слов

Обучим модель по всей тренировочной выборке и построим предсказания для тестовой
"""
X_train, y_train, scaler = lr_prepare_data(features, drop_categorical=True, use_encoded_heroes=True)
clf = LogisticRegression(penalty='l2', random_state=21, C=0.01)
clf.fit(X_train, y_train)

X_test, y_test, _ = lr_prepare_data(features_test, drop_categorical=True, use_encoded_heroes=True, scaler=scaler)
pred = clf.predict_proba(X_test)

print
print "Убедимся в разумности предсказаний"
print "Минимальное и максимальное значение предсказанных вероятностей"
print np.min(pred[:, :]), np.max(pred[:, :])
print
print "Убедимся, что модель не константная"
print "Подсчитаем кол-во одинаковых предсказаний"
print sum(np.absolute(pred[:,0] - pred[:,1]) < 0.00001)
print
print "Минимальное и максимальное вероятности победы команды radiant"
print np.min(pred[:, 1]), np.max(pred[:, 1])

f = open('predictions.csv', 'w')
f.write('match_id,radiant_win\n')

for i, match_id in enumerate(features_test.index):
    f.write(str(match_id) + ',' + str(pred[i, 1]) + '\n')
f.close()
