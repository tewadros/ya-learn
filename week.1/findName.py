def findName(string):	
    parenthesisPos = string.find('(')
    parWithQuote = string.find('("')
    if parenthesisPos > 0 and parWithQuote < 0:
        endNamePos = string.find(' ', parenthesisPos)
        if endNamePos < 0:
            endNamePos = string.find(')', parenthesisPos)
        name = string[parenthesisPos + 1 : endNamePos+1]
    else:
        dotPos = string.find('.') + 2
        spacePos = string.find(' ', dotPos)
        if spacePos < 0:
            name = string[dotPos : ]
        else:
            name = string[dotPos : string.find(' ', dotPos) + 1]

    return name
	
print findName('Ryerson, Miss. Emily Borie')
print findName('McGovern, Miss. Mary')
