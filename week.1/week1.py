from __future__ import division
import pandas as pd
import numpy as np
import re


def findName(string):
    res = re.search('\((\w+)[\s|\)]', string)
    if res is None:
        res = re.search('\.\s(\w+)', string)
    return res.group(1)



data = pd.read_csv('titanic.csv', index_col='PassengerId')

countBySex = data.Sex.value_counts();

print "1. Passengers count: males - ", countBySex['male'], " females = ", countBySex['female'] 

dataSetLength = data.shape[0]

print "2. Percent of survivors: ", np.sum(data.Survived) / dataSetLength

print "3. Percent of first class passengers: ", np.sum(data.Pclass == 1) / dataSetLength

print "4. Age mean: ", data.Age.mean(), " Age median: ", data.Age.median()

names = {}

survivors = 0

for id, item in data.iterrows():
    if item.Survived != 0:
        survivors += 1

print survivors, np.sum(data.Survived)


for id, item in data[data.Sex == 'female'].iterrows():

    name = findName(item.Name)

    if name not in names:
        names[name] = 0
    names[name] += 1

rows = []

for name in names:
    rows.append({'name': name, 'count': names[name]})

df = pd.DataFrame(rows)
print df.sort(columns = 'count').tail(3)
