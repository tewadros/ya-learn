from __future__ import division
import pandas as pd
import numpy as np
from sklearn import tree


data = pd.read_csv('titanic.csv', index_col='PassengerId')

data = data.loc[:,['Pclass', 'Fare', 'Age', 'Sex', 'Survived']].dropna(axis=0)

X = data.loc[:,['Pclass', 'Fare', 'Age', 'Sex']]
X.Sex = X.Sex == 'male'

Y = data.loc[:,['Survived']]

clf = tree.DecisionTreeClassifier()
clf.random_state = 241
clf = clf.fit(X, Y)

print clf.feature_importances_
