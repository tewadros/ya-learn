import pandas as pd
import re


def findNameRe(string):
    res = re.search('\((\w+)[\s|\)]', string)
    if res is None:
        res = re.search('\.\s(\w+)', string)
    return res.group(1)



data = pd.read_csv('titanic.csv', index_col='PassengerId')

for id, item in data[data.Sex == 'female'].iterrows():
    if item.Name.find('Mrs.') < 0:
        
        name = findNameRe(item.Name)
        if name == '':
            print 'not found'
            print id, item.Name
        elif name is None:
            print 'None'
            print id, item.Name
            
