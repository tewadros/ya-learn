import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import Ridge
from scipy.sparse import hstack
from sklearn.feature_extraction import DictVectorizer

print 'reading...'
data = pd.read_csv('salary-train.csv')

print 'preprocessing...'
data.FullDescription = data.FullDescription.str.lower()
data.FullDescription = data.FullDescription.replace('[^a-zA-Z0-9]', ' ', regex = True)

tfidfVectorizer = TfidfVectorizer(input='content', min_df=5)

data.ContractTime.fillna('nan', inplace=True)
data.LocationNormalized.fillna('nan', inplace=True)

enc = DictVectorizer()

features = hstack([
    tfidfVectorizer.fit_transform(data.FullDescription),
    enc.fit_transform(data[['LocationNormalized', 'ContractTime']].to_dict('records'))
])

print 'fitting...'
model = Ridge(alpha=1)

model.fit(features, data.SalaryNormalized)

print 'predicting...'
data_test = pd.read_csv('salary-test-mini.csv')

data_test.FullDescription = data_test.FullDescription.str.lower()
data_test.FullDescription = data_test.FullDescription.replace('[^a-zA-Z0-9]', ' ', regex = True)

testFeatures = hstack([
    tfidfVectorizer.transform(data_test.FullDescription),
    enc.transform(data_test[['LocationNormalized', 'ContractTime']].to_dict('records'))
])

print model.predict(testFeatures)
