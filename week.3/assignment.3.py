from __future__ import division
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
import sys

data_set = pd.read_csv('data-logistic.csv', header=None)

y, X = [np.array(data_set.loc[:,i]) for i in [0, (1,2)]]

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def costFunction(X, y, w):
    i = 0
    L = len(y)
    cost = 0.0
    while i < L:
        cost += np.log(1 / sigmoid(y[i] * X[i].dot(w)))
        i += 1
    return cost / L
    #return (np.log(1 + np.exp(-y * X.dot(w)))).sum() / len(y)

def costFunctionRegularized(X, y, w, C):
    return costFunction(X, y, w) + (w * w).sum() * C * 0.5

def grad(X, y, w):
    grad = np.array([0.0,0.0])
    L = len(y)
    i = 0
    for j in range(0, 2):
        while i < L:
            grad[j] += -1 * y[i] * X[i, j] * (1 - sigmoid(y[i] * X[i].dot(w)))
            i += 1
    return grad / L

def gradRegularized(X, y, w, C):
    g = grad(X, y, w)
    return g + C * w

def convergenceCriterium(w, wnew):
    return np.linalg.norm([w, wnew]) < 0.00001

w = np.array([0.0, 0.0])
n = 0
k = 0.1
C = 0
cost = np.array([])
while n < 10000:
    cost = np.append(cost, costFunctionRegularized(X, y, w, C))
    g = gradRegularized(X, y, w, C)
    wnew = w - k * g
    #print 'grad:   ', g
    #print 'weights:', w
    if convergenceCriterium(w, wnew):
        print 'converged! on ', n, 'iterations'
        break
    n += 1
    w = np.copy(wnew)


plt.plot(cost)
plt.show()

print w
unRegularizedScore = roc_auc_score(y, sigmoid(X.dot(w)))

w2 = np.array([0.0, 0.0])
n = 0
k = 0.1
C = 1
cost = np.array([])
while n < 10000:
    cost = np.append(cost, costFunctionRegularized(X, y, w2, C))
    g = gradRegularized(X, y, w2, C)
    w2 = w2 - k * g
    #print 'grad:   ', g
    #print 'weights:', w
    if convergenceCriterium(w, wnew):
        print 'converged! on ', n, 'iterations'
        break
    n += 1

plt.plot(cost)
plt.show()
print w2

regularizedScore = roc_auc_score(y, sigmoid(X.dot(w2)))

print unRegularizedScore, regularizedScore
