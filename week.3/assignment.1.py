from sklearn.svm import SVC
import pandas as pd

train_set = pd.read_csv('svm-data.csv', header=None, names=range(0, 3))

X_train = train_set.loc[:, 1:2]
y_train = train_set.loc[:, 0]

clf = SVC(C=100000, random_state=241, kernel='linear')

clf.fit(X_train, y_train)

print clf.support_
