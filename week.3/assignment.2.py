from sklearn import datasets
from sklearn.svm import SVC
from sklearn.feature_extraction.text import TfidfVectorizer
import numpy as np
from sklearn.cross_validation import KFold
from sklearn.grid_search import GridSearchCV

newsgroups = datasets.fetch_20newsgroups(
    subset='all', 
    categories=['alt.atheism', 'sci.space']
)

vectorizer = TfidfVectorizer()

X = vectorizer.fit_transform(newsgroups.data)

y = newsgroups.target

grid = {'C': np.power(10.0, np.arange(-5, 6))}
cv = KFold(y.size, n_folds=5, shuffle=True, random_state=241)
clf = SVC(kernel='linear', random_state=241)
gs = GridSearchCV(clf, grid, scoring='accuracy', cv=cv)
gs.fit(X, y)

for a in gs.grid_scores_:
    print a.mean_validation_score
    print a.parameters

clf = SVC(kernel='linear', random_state=241, C=1)

clf.fit(X, y)

words = []
for i in np.argsort(np.absolute(np.asarray(clf.coef_.todense()).reshape(-1)))[-10:]:
    words.append(vectorizer.get_feature_names()[i])

words.sort()

for word in words:
    print word,
