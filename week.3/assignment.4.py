from __future__ import division
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
import sklearn.metrics as met

data_set = pd.read_csv('classification.csv')

tp = sum((data_set.true == 1) & (data_set.pred == 1))
fp = sum((data_set.true == 0) & (data_set.pred == 1))
tn = sum((data_set.true == 0) & (data_set.pred == 0))
fn = sum((data_set.true == 1) & (data_set.pred == 0))
print tp, fp, fn, tn

f = open('submission.4.1', 'w')
f.write(str(tp) + ' ' + str(fp) + ' ' + str(fn) + ' ' + str(tn))
f.close()

metrics = ['accuracy_score', 'precision_score', 'recall_score', 'f1_score']

f = open('submission.4.2', 'w')

for method in metrics:
    score = round(getattr(met, method)(data_set.true, data_set.pred), 2)
    print method, score
    f.write(str(score) + ' ')

f.close()

data_set = pd.read_csv('scores.csv')

f = open('submission.4.3', 'w')

best_score = 0
best_column = ''
columns = ['score_logreg', 'score_svm', 'score_knn', 'score_tree']
for column in columns:
    roc_auc = met.roc_auc_score(data_set.true, data_set[column])
    print column, roc_auc
    if (best_score < roc_auc):
        best_score = roc_auc
        best_column = column

f.write(best_column)
f.close()

f = open('submission.4.4', 'w')

best_precision = 0
best_column = ''
columns = ['score_logreg', 'score_svm', 'score_knn', 'score_tree']
for column in columns:
    p, r, t = met.precision_recall_curve(data_set.true, data_set[column])
    p_at_70_r = max(p[r >= 0.7])
    print column, p_at_70_r
    if (best_precision < p_at_70_r):
        best_precision = p_at_70_r
        best_column = column

f.write(best_column)
f.close()



