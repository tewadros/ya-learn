from __future__ import division
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import roc_auc_score
import sys

data_set = pd.read_csv('data-logistic.csv', header=None)

y, X = [np.array(data_set.loc[:,i]) for i in [0, (1,2)]]

X1 = X[:, 0]
X2 = X[:, 1]

w1 = w2 = 0.0
w1_new = w2_new = 0.0
k = 0.1
C = 0

n = 0

eucl = 1

while (eucl >= 0.00001) & (n < 10000):
    w1_new = w1 + k * sum(y * X1 * (1 - 1 / (1 + np.exp(-y*(X1*w1 + X2*w2))))) / len(y) - k * C * w1
    w2_new = w2 + k * sum(y * X2 * (1 - 1 / (1 + np.exp(-y*(X1*w1 + X2*w2))))) / len(y) - k * C * w2
    eucl = ((w1_new - w1) ** 2 + (w2_new - w2) ** 2) ** 0.5
    n += 1
    w1 = w1_new
    w2 = w2_new

print 'converged in ', n, 'iteration(s)'
print C, n, w1, w2

print round(roc_auc_score(y, 1 / (1 + np.exp(-1 *(X1*w1 + X2*w2)))), 3)



w1 = w2 = 0.0
w1_new = w2_new = 0.0
k = 0.1
C = 10

n = 0

eucl = 1

while (eucl >= 0.00001) & (n < 10000):
    w1_new = w1 + k * sum(y * X1 * (1 - 1 / (1 + np.exp(-y*(X1*w1 + X2*w2))))) / len(y) - k * C * w1
    w2_new = w2 + k * sum(y * X2 * (1 - 1 / (1 + np.exp(-y*(X1*w1 + X2*w2))))) / len(y) - k * C * w2
    eucl = ((w1_new - w1) ** 2 + (w2_new - w2) ** 2) ** 0.5
    n += 1
    w1 = w1_new
    w2 = w2_new

print 'converged in ', n, 'iteration(s)'
print C, n, w1, w2

print round(roc_auc_score(y, 1 / (1 + np.exp(-1 * (X1*w1 + X2*w2)))), 3)
