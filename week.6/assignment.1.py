from skimage import io
from skimage import img_as_float
from sklearn.cluster import KMeans
import numpy as np

def PSNR(Xo, Xt):
    MSE = np.mean((Xo - Xt) ** 2)
    return 10 * np.log10(1.0 / MSE)


image = img_as_float(io.imread('parrots.jpg'))

X = np.array([pixel for row in image for pixel in row])

for n_colors in range(20, 7, -1):
    clf = KMeans(n_clusters= n_colors, init='k-means++', random_state=241)
    clf = clf.fit(X)
    p = clf.predict(X)

    X_median = X.copy()
    X_mean = X.copy()

    for i in range(clf.n_clusters):
        X_median[p == i] = np.median(X[p == i], axis=0)
        X_mean[p == i] = np.mean(X[p == i], axis=0)

    print n_colors,
    print 'psnr by median:', PSNR(X, X_median),
    print 'psnr by mean:  ', PSNR(X, X_mean)

#io.imshow(X_median.reshape(image.shape))
#io.show()
#io.imshow(X_mean.reshape(image.shape))
#io.show()

