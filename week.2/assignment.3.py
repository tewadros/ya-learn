import pandas as pd
from sklearn.linear_model import Perceptron
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import StandardScaler

train_set = pd.read_csv('D:/Study/ya/week.2/perceptron-train.csv', header=None, names=range(0, 3))

X_train = train_set.loc[:, 1:2]
y_train = train_set.loc[:, 0]

clf = Perceptron(random_state=241)
clf = clf.fit(X_train, y_train)

test_set = pd.read_csv('D:/Study/ya/week.2/perceptron-test.csv', header=None, names=range(0, 3))

X_test = test_set.loc[:, 1:2]
y_test = test_set.loc[:, 0]

predictions = clf.predict(X_test)

accuracy = accuracy_score(y_test, predictions)

scaler = StandardScaler()

X_train_scaled = scaler.fit_transform(X_train)

clf = clf.fit(X_train_scaled, y_train)

X_test_scaled = scaler.transform(X_test)

predictions = clf.predict(X_test_scaled)

accuracy_scaled = accuracy_score(y_test, predictions)

print accuracy_scaled - accuracy

f = open('submission.3.1', 'w')
f.write(str(accuracy_scaled - accuracy))
f.close()

