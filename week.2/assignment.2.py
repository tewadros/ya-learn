from sklearn.datasets import load_boston
from sklearn import preprocessing
import numpy as np
from sklearn.cross_validation import KFold
from sklearn.neighbors import KNeighborsRegressor
from sklearn.cross_validation import cross_val_score

boston = load_boston()

X = preprocessing.scale(boston.data)
y = boston.target

p_range = np.linspace(start=1, stop=10, num=200)

cv = KFold(y.size, 5, shuffle=True, random_state=42)

knr = KNeighborsRegressor(n_neighbors=5, weights='distance')


max_score = -100000
p_max = 0

points = []

for p in p_range:
    knr.p = p
    score = cross_val_score(knr, X, y, scoring='mean_squared_error', cv=cv).mean()
    
    if score > max_score:
        max_score = score
        p_max = p

print "best p:", p_max, "mean_squared_error:", max_score


