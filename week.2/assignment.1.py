import pandas as pd
from sklearn.cross_validation import KFold
from sklearn.neighbors import KNeighborsClassifier
from sklearn import preprocessing
from sklearn.cross_validation import cross_val_score


data = pd.read_csv('/home/sarkisyan/ya/week 2/wine.data', names=range(0,14))

y = data.loc[:,0]
X = data.loc[:,1:13]

cv = KFold(y.size, 5, shuffle=True, random_state=42)

knn = KNeighborsClassifier()

max_score = 0
n_max = 0

for n in range(1, 51):
    knn.n_neighbors = n
    score = cross_val_score(knn, X, y, scoring='accuracy', cv=cv).mean()
    if score > max_score:
        max_score = score
        n_max = n

print "best n:", n_max, "accuracy:", max_score

max_score = 0
n_max = 0

X_scaled = preprocessing.scale(X)

for n in range(1, 51):
    knn.n_neighbors = n
    score = cross_val_score(knn, X_scaled, y, scoring='accuracy', cv=cv).mean()
    if score > max_score:
        max_score = score
        n_max = n

print "after scaling input"
print "best n:", n_max, "accuracy:", max_score

