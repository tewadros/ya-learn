from sklearn.ensemble import RandomForestRegressor
import pandas as pd
from sklearn.cross_validation import KFold
from sklearn.metrics import r2_score

data = pd.read_csv('abalone.csv')

y = data.Rings
data.Sex = data.Sex.map(lambda x: 1 if x == 'M' else (-1 if x == 'F' else 0))
X = data.drop(['Rings'], axis=1)

kf = KFold(y.size, 5, shuffle=True, random_state=1)

max_score = 0
n_max = 0

clf = RandomForestRegressor(random_state=1)

for n in range(1, 51):
    clf.n_estimators = n
    score = 0
    for train, test in kf:
        clf.fit(X.loc[train], y.loc[train])
        score += r2_score(y.loc[test], clf.predict(X.loc[test]))
    score /= 5
    
    if score > max_score:
        max_score = score
        n_max = n
    print n, score

print "best n:", n_max, "accuracy:", max_score
