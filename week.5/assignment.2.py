from sklearn.ensemble import GradientBoostingClassifier
import pandas as pd
import numpy as np
from sklearn.cross_validation import train_test_split
from sklearn.metrics import log_loss
import matplotlib.pyplot as plt
#%matplotlib inline

data = pd.read_csv('gbm-data.csv')

y = data.Activity.values
X = data.drop(['Activity'], axis=1).values

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.8, random_state=241)

gb = GradientBoostingClassifier(random_state=241, n_estimators=250, verbose=True)

for learningRate in [0.2]:#[1, 0.5, 0.3, 0.2, 0.1]:
    gb.learning_rate = learningRate
    gb.fit(X_train, y_train)

    log_loss_train = []

    for y_pred in gb.staged_decision_function(X_train):
        log_loss_train.append(log_loss(y_train, 1/(1 + np.exp(-y_pred))))

    log_loss_test = []

    for y_pred in gb.staged_decision_function(X_test):
        log_loss_test.append(log_loss(y_test, 1/(1 + np.exp(-y_pred))))
    
    plt.figure()
    plt.plot(log_loss_test, 'r', linewidth=2)
    plt.plot(log_loss_train, 'g', linewidth=2)
    plt.legend(['test', 'train'])
    plt.show()
